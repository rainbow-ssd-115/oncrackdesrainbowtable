//
// Created by yizhu on 9/28/22.
//


#include <iostream>
#include <algorithm>
#include "crack.h"

int main(int argc, char *argv[]) {
    if (argc == 3) {
        std::string passwordLength;
        std::string hashedPwdFilePath;

        passwordLength = argv[1];
        auto mainCrack = new crack();
        std::vector<std::string> listLengthPassword{"6","7","8","9","10"};
        if (std::find(std::begin(listLengthPassword), std::end(listLengthPassword), passwordLength) != std::end(listLengthPassword)) {
            mainCrack->rainbowTablePath = "rainbowTable" + passwordLength + ".dat";
            hashedPwdFilePath = argv[2];
            mainCrack->findHashes(hashedPwdFilePath,stoi(passwordLength));
        }
        else{
            std::cout << "Password length not in range 6-10";
        }
        delete mainCrack;
    }
    else{
        std::cout << "Incorrect number of arguments";
    }

}
