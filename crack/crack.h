//
// Created by yizhu on 9/27/22.
//

#ifndef SSD_CRACK_H
#define SSD_CRACK_H

#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <future>
#include "../Utils/sha256.h"
#include "../Utils/UnsignedString.h"

class crack {
public:
    std::vector<UnsignedString> leakedHashes;
    std::vector<std::future<std::string>> result;
    std::vector<SHA256> sha256;
    std::string rainbowTablePath;

    void init(int passwordLength);
    void findHashes(std::string hashedPwdFilePath, int passwordLength);

    static inline std::string
    findHash(UnsignedString leakedHash, const std::string &rainbowTablePath, int passwordLength);

    inline void setArrayOfLeakedHash(std::string &leakedHashPath);

    static std::string
    checkQueue(unsigned char hashedPwdToFind[32], const std::string &rainbowTablePath, int passwordLength);

    static std::string
    isInChain(std::string pwd, UnsignedString leakedHash,int &position, int pwdLength);

    std::string test(unsigned char mdp[32], int i);

    void writeResult();
};

#endif //SSD_CRACK_H
