//
// Created by yizhu on 9/27/22.
//

#include "crack.h"
#include "../Utils/passwd-utils.hpp"
#include "../Utils/threadpool.hpp"
#include "../Utils/reduce.hpp"

const std::string NOTFOUND = "not found";
int NUMBER_OF_REDUCTION = 0;

void crack::init(int passwordLength){
    switch (passwordLength) {
        case 6 : NUMBER_OF_REDUCTION = 5000;break;
        case 7 : NUMBER_OF_REDUCTION = 10000;break;
        case 8 : NUMBER_OF_REDUCTION = 30000;break;
    }
}
void crack::writeResult() {

    std::ofstream myfile("result.txt");

    if (myfile.is_open()) {
        std::string str;
        long unsigned int i = 0;
        do {
            str = result.at(i).get();
            myfile << str << std::endl;
            i++;
        } while (i < leakedHashes.size());
        myfile.close();
    } else std::cerr << "Unable to open file";
}

void crack::findHashes(std::string hashedPwdFilePath, int passwordLength) {
    this->setArrayOfLeakedHash(hashedPwdFilePath);
    init(passwordLength);
    ThreadPool t;
    for (auto leakedHashe: leakedHashes) {
        result.push_back(t.enqueue(findHash, leakedHashe, rainbowTablePath, passwordLength));
    }
    writeResult();

}

std::string
crack::findHash(UnsignedString leakedHash, const std::string &rainbowTablePath, int passwordLength) {
    int reductionNumber = 0;
    unsigned char hashedPwdToFind[32];
    std::string reducedPwd;
    std::string result;
    SHA256 sha;
    do {
        for (unsigned int i = 0; i < 32; i++) {
            hashedPwdToFind[i] = leakedHash[i];
        }
        if (reductionNumber != 0) {
            for (int ptr = NUMBER_OF_REDUCTION - reductionNumber; ptr < NUMBER_OF_REDUCTION; ptr++) {
                reducedPwd = rainbow::reduceHash3(hashedPwdToFind,ptr,passwordLength);
                //reducedPwd = reduce.reduceHashCrack(hashedPwdToFind, ptr, passwordLength);
                sha.getRawHash(reducedPwd, hashedPwdToFind);
            }
        }
        std::string chainHead = checkQueue(hashedPwdToFind, rainbowTablePath, passwordLength);
        //check hash in chain
        if (chainHead != NOTFOUND) {
            result = isInChain(chainHead, leakedHash,reductionNumber, passwordLength);
        } else {
            result = NOTFOUND;
        }
    } while (result == NOTFOUND && ++reductionNumber <= NUMBER_OF_REDUCTION);
    return result;
}

void crack::setArrayOfLeakedHash(std::string &leakedHashPath) {
    unsigned long c;
    std::ifstream leakedHashFile;
    leakedHashFile.open(leakedHashPath);
    std::string leakedHash;
    std::string tempt;

    while (getline(leakedHashFile, leakedHash)) {
        UnsignedString bytes{};
        for (unsigned int i = 0; i < 32; i++) {
            tempt.push_back(leakedHash[i * 2]);
            tempt.push_back(leakedHash[i * 2 + 1]);
            std::istringstream hex_chars_stream(tempt);
            hex_chars_stream >> std::hex >> c;
            bytes[i] = c;
            tempt.clear();
        }
        leakedHashes.push_back(bytes);
    }
    leakedHashFile.close();
}

std::string
crack::checkQueue(unsigned char hashedPwdToFind[32], const std::string &rainbowTablePath, int passwordLength) {
    std::ifstream rainbowTableStream;

    rainbowTableStream.open(rainbowTablePath, std::ios::in | std::ios::binary);
    // start read at
    int blockSize = 32 + passwordLength;

    // get length of file:
    rainbowTableStream.seekg(0, std::ifstream::end);
    long int blockNumber = rainbowTableStream.tellg() / blockSize;
    rainbowTableStream.seekg(0, std::ifstream::beg);

    long int left = std::ifstream::beg;
    long int right = blockNumber;
    unsigned char headHexa[passwordLength];
    auto *tailHexa = new unsigned char[32];

    while (left <= right) {
        long int mid = left + (right - left) / 2;
        rainbowTableStream.seekg(std::ifstream::beg);
        rainbowTableStream.seekg((mid * blockSize)+passwordLength);
        //rainbowTableStream.read(reinterpret_cast<char *>(headHexa), passwordLength);
        rainbowTableStream.read(reinterpret_cast<char *>(tailHexa), 32);
        if (memcmp(tailHexa, hashedPwdToFind, 32) == 0) {
            rainbowTableStream.seekg((mid * blockSize));
            rainbowTableStream.read(reinterpret_cast<char *>(headHexa), passwordLength);
            rainbowTableStream.close();
            delete[] tailHexa;
            return reinterpret_cast<const char *>(headHexa);
        } else if (memcmp(tailHexa, hashedPwdToFind, 32) < 0) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    rainbowTableStream.close();
    delete[] tailHexa;
    return NOTFOUND;
}

std::string
crack::isInChain(std::string pwd, UnsignedString leakedHash,int &position, int pwdLength) {
    unsigned char hashedPwd[32];
    bool flag = false;
    std::string reducedPwd = pwd;
    SHA256 sha;
    for (int i = 0; i< NUMBER_OF_REDUCTION-position;i++) { // change to constant
        sha.getRawHash(reducedPwd, hashedPwd);
        // for(char c: hashedPwd ){
        //     printf("%02x",c);
        // }
        // printf("\n");

        reducedPwd = rainbow::reduceHash3(hashedPwd,i,pwdLength);
        //reducedPwd = reduce.reduceHashCrack(hashedPwd, i, pwdLength);

    }
    sha.getRawHash(reducedPwd, hashedPwd);

    if (memcmp(hashedPwd, leakedHash.c_str(), 32) == 0) {
        flag = true;
    }
    return flag ? reducedPwd : NOTFOUND;
}
