SUBDIRS = crack generate Utils

.PHONY: run_subdir $(SUBDIRS)
run_subdir: ${SUBDIRS}

${SUBDIRS}:
	$(MAKE) -C $@

clean-crack:
	$(MAKE) clean -C crack clean

clean-generate:
	$(MAKE) clean -C generate clean

clean-Utils:
	$(MAKE) clean -C Utils clean

clean-dat:
	rm -f *.dat