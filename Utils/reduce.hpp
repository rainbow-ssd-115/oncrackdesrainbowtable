#ifndef REDUCE_HPP
#define REDUCE_HPP

#include <string>
#include <bitset>

#include "sha256.h"
#include <cstdint>
#include <unordered_map>
#include <bitset>
#include <cmath>
#include <fstream>
namespace rainbow
{

    const std::string charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    const uint8_t charsetLength = 62;

/**
 * @brief reduceHash Reduit le hash en fonction de sa position dans la chaine de
 *        reduction/hash.
 * @param hashString Le hash que nous voulons réduire.
 * @param columnIndex La colonne dans la chaine de reduction/hash.
 * @param pswdLength Longueur du mdp.
 * @return renvoie le hash réduit.
 */
/*inline std::string  reduceHash(unsigned char hash[SHA256::HashBytes],
                               const uint32_t & columnIndex, const uint8_t & pswdLength)
{
    std::string result = "";
    for (uint8_t i = 0; i < pswdLength; i++)
    {
        result += charactset[((((uint8_t) hashString.at((
                                                 i + columnIndex) % 64))) % 62)];
    }
    return result;
}*/
void reduceHash3(unsigned char hashRaw[SHA256::HashBytes], char * result,const uint16_t &columnIndex, const uint8_t &pswdLength) {
      int randomColumn;
      for (int i = 1; i < pswdLength + 1; i++) {
          randomColumn = static_cast<int>(((((columnIndex * i * i) >> 11) + hashRaw[i] + hashRaw[32 - i] * pswdLength) & 63) + columnIndex) % 62;
          result[i-1]= charset[randomColumn];
      }
  }

std::string reduceHash3(unsigned char hashRaw[SHA256::HashBytes], const uint16_t &columnIndex, const uint8_t &pswdLength) {
      std::string result;
      int randomColumn;
      for (int i = 1; i < pswdLength + 1; i++) {
          randomColumn = static_cast<int>(((((columnIndex * i * i) >> 11) + hashRaw[i] + hashRaw[32 - i] * pswdLength) & 63) + columnIndex) % 62;
          result += charset[randomColumn];
      }
      return result;
  }


//    std::string reduceHash4(unsigned char hashRaw[SHA256::HashBytes], uint32_t columnIndex, uint8_t pswdLength) {
//        std::string result = "";
//        //std::bitset<8> b =hashRaw[columnIndex%SHA256::HashBytes];;
//        int jump = (static_cast<int>(hashRaw[columnIndex % SHA256::HashBytes]) + columnIndex) % charsetLength;
//
//        while (pswdLength > 0) {
//            int alphaPos = static_cast<int>(hashRaw[columnIndex % SHA256::HashBytes]) % charsetLength;
//            result += charset[alphaPos % charsetLength];
//            pswdLength--;
//            columnIndex = (columnIndex + jump);
//        }
//        return result;
//    }


    /**
     * @brief reduceHash5 reduce function, start with the first 8 bits at the columnIndex position of the hashRaw char array and continue with each next
     * @param hashRaw a char array reprensenting the hash in a hex format
     * @param columnIndex the column index
     * @param pswdLength the password size
     * @return
     */
//    std::string reduceHash5(unsigned char hashRaw[SHA256::HashBytes], int &columnIndex, int pswdLength) {
//        std::string result = "";
//        int index = columnIndex + 1;
//        //std::bitset<8> b =hashRaw[columnIndex%SHA256::HashBytes];;
//        while (pswdLength > 0) { //while the pass is not defined
//            float air = sqrt(M_PI * index * sqrt(pow(float(columnIndex), 2) + pow(sqrt(
//                                                                                          static_cast<float>(hashRaw[index % SHA256::HashBytes]) + index),
//                                                                                  2)));// compute the position of the alphanumeric character to select in characterset.
//            // the position is calculated by taking the hash hex at the 'index' position and cast it into a float before mutiplication it with (100/columnIndex)
//            int position = int(air * 100 - (floor(air)) * 100); // take the first 2 digits after the decimal point.
//            //std::cout <<"indeix : "<< index <<" ||| "<< "air : "<< air<<" ||| position : "<< position << "\n";
//            //std::cout<<"int : "<<a << "\n";
//            result += charset[position % charsetLength];
//            pswdLength--;
//            index = (index + static_cast<int>(charset[position % charsetLength]));
//        }
//        return result;
//    }
     template<size_t size>
    class Reduce {
    private:
        unsigned int currentPosition = 0;
        unsigned char bitmap[size];
    public:
        Reduce(std::string filename){
            std::ifstream file;
            file.open(filename,std::ios::in|std::ios::binary);
            if (file.is_open()){
                file.read((char *)&bitmap,(int)size*sizeof(unsigned char));
            }
            file.close();
        }
        void reduceHash(unsigned char hashRaw[SHA256::HashBytes],int columnIndex, char * result, int pssLength){
            columnIndex++;
            uint32_t nextBitmapPosition =(uint32_t) (hashRaw[columnIndex%SHA256::HashBytes]<<24 | hashRaw[(columnIndex+1+(columnIndex/SHA256::HashBytes))%SHA256::HashBytes]<<16 | hashRaw[(columnIndex+2+(2*(columnIndex/SHA256::HashBytes)))%SHA256::HashBytes]<< 8 | hashRaw[(columnIndex+3+(3*(columnIndex/SHA256::HashBytes)))%SHA256::HashBytes]);
            int nextHashPosition = columnIndex % SHA256::HashBytes;
            int HashJump = (columnIndex/SHA256::HashBytes)%SHA256::HashBytes;
            int bitmapJump = columnIndex;
            unsigned char d;

            for (int i = 0; i<(int)pssLength;i++){
                //jump = (int) bitmap[position%size];
                d = bitmap[(nextBitmapPosition)%(int)size] & hashRaw[(int)(bitmap[(nextBitmapPosition)%(int)size])%SHA256::HashBytes] ; // 5A -> 90 % 58 = 32
                result[i] = charset[((int) d)%charsetLength]; // xxx
                nextBitmapPosition += bitmapJump; // +0
                nextHashPosition += HashJump; // 0
                //position += jump;
            }
            //result[size] = '\0';
        }

        std::string reduceHashCrack(unsigned char hashRaw[SHA256::HashBytes],int columnIndex, int pssLength){
            columnIndex++;
            uint32_t nextBitmapPosition =(uint32_t) (hashRaw[columnIndex%SHA256::HashBytes]<<24 | hashRaw[(columnIndex+1+(columnIndex/SHA256::HashBytes))%SHA256::HashBytes]<<16 | hashRaw[(columnIndex+2+(2*(columnIndex/SHA256::HashBytes)))%SHA256::HashBytes]<< 8 | hashRaw[(columnIndex+3+(3*(columnIndex/SHA256::HashBytes)))%SHA256::HashBytes]);
            int nextHashPosition = columnIndex % SHA256::HashBytes;
            int HashJump = (columnIndex/SHA256::HashBytes)%SHA256::HashBytes;
            int bitmapJump = columnIndex;
            unsigned char d;
            std::string result;

            for (int i = 0; i<(int)pssLength;i++){
                //jump = (int) bitmap[position%size];
                d = bitmap[(nextBitmapPosition)%(int)size] & hashRaw[(int)(bitmap[(nextBitmapPosition)%(int)size])%SHA256::HashBytes] ; // 5A -> 90 % 58 = 32
                result += charset[((int) d)%charsetLength]; // xxx
                nextBitmapPosition += bitmapJump; // +0
                nextHashPosition += HashJump; // 0
                //position += jump;
            }
            //result[size] = '\0';
            return result ;
        }
    };
        
}
#endif // REDUCE_HPP
