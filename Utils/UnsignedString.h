#ifndef SSD_UNSIGNEDSTRING_H
#define SSD_UNSIGNEDSTRING_H


class UnsignedString {
private:
    unsigned char data[32];
public:
    UnsignedString() = default;

    unsigned char& operator [](unsigned i)
    {
        return data[i];
    }
    const unsigned char* c_str() const
    {
        return data;
    }
};


#endif //SSD_UNSIGNEDSTRING_H
