<h1 align="center">
  <br>
  <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fipwithease.com%2Fwp-content%2Fuploads%2F2021%2F01%2Frainbow-table-dp-768x450.jpg&f=1&nofb=1&ipt=16c6a714ba34b1d7befe76a1bc69b8d8d309f70cd772bc418d229bb1c5c54a1a&ipo=images" alt="Markdownify" width="200">
  <br>
</h1>

<p align="center">
  <a href="#how-to-build">How to build</a> •
  <a href="#how-to-clean">How to clean</a> •
  <a href="#how-to-use">How to use</a> •
  <a href="#generation-rainbow-table">Generation</a> •
  <a href="#crack">Crack</a> •
  <a href="#verification">Verification</a>
</p>

 - Yi ZHU 000497228
 - Valentin KOLA 000482170
 - Valentin DE BAENE 000498576
 - Arnaud MALCHAIR 000572292
 - Botond Horac HORVATH 000440356

<span style="color:red">All commands should be executed in directory : "oncrackdesrainbowtable"</span>

# How to build

```bash
$ make
```
# How to clean
 
### clean executable in folder generate

```bash
$ make clean-generate
```
### clean executable in folder crack

```bash
$ make clean-crack
```
### clean executable in folder Utils

```bash
$ make clean-Utils
```

# How to use

### Generation rainbow table

```bash
$ ./generate/generateRainbowTable.out
```


### Crack

```bash
$./crack/crackTable.out l <file>
```
 - l : length of password to crack (6-7-8)
 - file : file which contains the hashes we want to crack

### Verification

```bash
$./Utils/check-passwd.out result.txt <file>
```
 - file : file which contains the hashes we want to crack
 - result.txt : file we generate where we write the result of the crack

