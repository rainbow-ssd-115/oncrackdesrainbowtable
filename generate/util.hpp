#ifndef UTILS_HPP
#define UTILS_HPP
#include "../Utils/sha256.h"

namespace rainbow {

uint64_t gigabyte = 1073741824;

template<int size>
struct Chain
{
    char head[size];
    unsigned char tail [SHA256::HashBytes];

    bool operator<(const struct Chain& secondChain) const{
            for (uint8_t i = 0; i< SHA256::HashBytes; i++)
            {
                if (tail[i] != secondChain.tail[i])
                {
                    return tail[i]>secondChain.tail[i];
                }
            }
            return true;
        }

};
}


#endif // UTILS_HPP
