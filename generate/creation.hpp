#ifndef CREATION_HPP
#define CREATION_HPP
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <queue>
#include "../Utils/sha256.h"
#include "../Utils/passwd-utils.hpp"
#include "../Utils/reduce.hpp"
#include "../Utils/threadpool.hpp"
#include "util.hpp"
#include "map"

namespace rainbow
{
const int arrayMaxSize = 10000;
const int maxFileSize = 11; // max 11gb
const int hexLength = 16;

template<int size>
class Creation {
public:
    inline static bool running = true;
    inline static std::mutex allMutex[hexLength];

    inline static std::ofstream out_files[hexLength];
    inline static std::ifstream in_files[hexLength];
    inline static std::ofstream result_file;
    inline static bool file_sorted[hexLength];

    inline static std::string filesName[hexLength] = {"hash0.dat","hash1.dat","hash2.dat","hash3.dat","hash4.dat","hash5.dat",
                                                      "hash6.dat","hash7.dat","hash8.dat","hash9.dat","hashA.dat","hashB.dat",
                                                      "hashC.dat","hashD.dat","hashE.dat","hashF.dat"};

    static void open_out_files(){
        for (int i = 0;i<hexLength;i++){
            out_files[i].open(filesName[i].c_str(),std::ios::out|std::ios::binary);
        }
    }

    static void close_out_files(){
        for (int i = 0;i<hexLength;i++){
            out_files[i].close();
        }
    }

    static void remove_out_files(){
        for (int i = 0; i<hexLength;i++){
            remove(filesName[i].c_str());
        }
    }

    inline static bool is_full(int & sizeTogenerate){
        int files_size = 0;
        for (int i = 0;i<hexLength;i++){
            files_size += out_files[i].tellp();
        }
        return (files_size > sizeTogenerate);
    }

    /**
     * generate a chain of sha256 + reduction 'length' times
     * @param length the length of the chain
     * @param size pass size
     * @return the hash tail of the chain
     */
    static void generate_chain(int length)
    {
        Chain<size> buffers[hexLength][arrayMaxSize];

        int iterators[hexLength] = {};
        SHA256 sha;
        Chain<size> chain;
        char pass[size];
        unsigned char hash[SHA256::HashBytes];
        int8_t it16 ;
        while (running){
            generate_passwd<size>(chain.head);
            memcpy(pass,chain.head,size);
            for (int i=0;i<length;i++){
               sha.getRawHash<size>(pass,hash);
               reduceHash3(hash,pass,i,size);
            }
            it16 = sha.getFinalRawHash<size>(pass,chain.tail);
            if (iterators[it16] == arrayMaxSize){
                allMutex[it16].lock();
                if (out_files[it16].is_open())
                {
                    out_files[it16].write((char *)&(buffers[it16]),(iterators[it16])*(SHA256::HashBytes+size));
                }
                allMutex[it16].unlock();
                iterators[it16] = 0;
            }
            buffers[it16][iterators[it16]++] = chain;
        }
        for (int i = 0;i<hexLength;i++){
            allMutex[i].lock();
            if (out_files[i].is_open())
            {
                out_files[i].write((char *)&(buffers[i]),(iterators[i])*(SHA256::HashBytes+size));
            }
            allMutex[i].unlock();
        }
    }

    static void sorting_file(int &index, std::ofstream &result_file){
        std::priority_queue<Chain<size>,std::vector<Chain<size>>> chains;
        int blockSize = SHA256::HashBytes + size;
        if (in_files[index].is_open()){
            in_files[index].seekg(0, std::ifstream::end);
            long int blockNumber = in_files[index].tellg() / blockSize;
            in_files[index].seekg(0, std::ifstream::beg);
            Chain<size> c;
            while ( blockNumber > 0){
                in_files[index].read((char *)&c,SHA256::HashBytes + size );
                chains.push(c);
                blockNumber --;
            }
        }
        Chain<size> prev;
        bool myTurn = false;
        
        while (!myTurn){
            myTurn = true;
            for (int i = 0; i<index;i++){
                if (!file_sorted[i]){
                    myTurn = false;
                    break;
                }
            }
            if (!myTurn){
                sleep(1);
            }
        }

        allMutex[0].lock();
        if (result_file.is_open() && !chains.empty()){
            while (!chains.empty()){
                prev = chains.top();
                result_file.write((char *)&chains.top(),SHA256::HashBytes+size);
                chains.pop();
                while (prev.tail == chains.top().tail){
                    chains.pop();
                }
            }
        }
        allMutex[0].unlock();
        file_sorted[index] = true;
    }

    static bool merging_files(int numFile){
        in_files[numFile].open(filesName[numFile].c_str(),std::ios::in|std::ios::binary);
        sorting_file(numFile,result_file);
        in_files[numFile].close();
        return true;
    }
    /**
     * generate passwords and their chain in order to store the head and the tail in a rainbow table file
     * @param n size of the password
     * @param size size of the table
     * @param length length of the chain
    */
    void mass_generate(int length, int sizeToGenerate, std::chrono::steady_clock::time_point begin)
    {
        // std::vector<std::future<rainbow::Chain>> chains;
        open_out_files();
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        int nbThreads = std::thread::hardware_concurrency();
        ThreadPool t = ThreadPool(nbThreads); // threads construction
        for (int i = 0; i<nbThreads;i++){
           t.enqueue(this->generate_chain, length);
        }
        while (!is_full(sizeToGenerate) && (std::chrono::duration_cast<std::chrono::hours>(end - begin).count() < 12)  ){
            sleep(60);
            end = std::chrono::steady_clock::now();
        }
        //sleep(60*60);
        running = false;
        t.stop();
        ThreadPool t2 = ThreadPool(nbThreads);
        close_out_files();
        result_file.open("rainbowTable"+std::to_string(size)+".dat",std::ios::out|std::ios::binary);
        std::vector<std::future<bool>> res ={} ;
        for (int i = 0; i<hexLength;i++){
            res.push_back(t2.enqueue(this->merging_files,i));
        }
        for (int i = 0; i < hexLength; i ++){
            res.at(i).get();
        }
        t2.stop();
        result_file.close();
        remove_out_files();
    }
};

}
#endif // CREATION_HPP
