
#include <unistd.h>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <bitset>
#include <fstream>
#include <cstdlib>
#include "creation.hpp"
#include "bitmapGenerator.hpp"
 #include <iostream>
#include <set>
#include <chrono>


int main()
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::cout << "RainbowTable 6 generation ..." << std::endl;
    constexpr int n = 6;
    int l = 5000;
    rainbow::Creation<n> creation6;
    long int sizeToGenerate = 1200000000; // 1.2GB
    creation6.mass_generate(l, sizeToGenerate, begin);
    std::cout << "RainbowTable 7 generation ..." << std::endl;
    // pass 7
    constexpr int o = 7;
    rainbow::Creation<o> creation7;
    l = 10000;
    sizeToGenerate = 12000000000; // 12GB
    creation7.mass_generate(l, sizeToGenerate, begin);
    std::cout << "RainbowTable 8 generation ..." << std::endl;
    // pass 8
    constexpr int p = 8;
    rainbow::Creation<p> creation8;
    l = 30000;
    sizeToGenerate = 12000000000;//12GB
    creation8.mass_generate(l, sizeToGenerate, begin);



    return 0;
}

