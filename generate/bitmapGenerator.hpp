#ifndef BITMAPGENERATOR_HPP
#define BITMAPGENERATOR_HPP
#include <cstdio>
#include <cstdlib>
#include <string>
#include <fstream>
namespace bitmapGenerator {

template< size_t size>
void bitmapGeneration( std::string filename) {
    std::ofstream file;
    file.open(filename,std::ios::out|std::ios::binary);
    unsigned char bits[(int)size];
    for (int i = 0 ; i< (int)size;i++){
        bits[i] = (unsigned char) rand();
    }
    file.write((char *)&bits,(int)size*sizeof(unsigned char));

}
}
#endif // BITMAPGENERATOR_HPP
